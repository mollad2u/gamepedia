<?php
/**
 * Created by PhpStorm.
 * User: gauth
 * Date: 12/03/2017
 * Time: 11:51
 */
namespace gamepedia\Models;


class Rating extends  \Illuminate\Database\Eloquent\Model {

    protected $table = 'game_rating';
    protected $primaryKey = 'id';
    public $timestamps = false;


    function Game(){

        return $this->belongsToMany('gamepedia\Models\Game','game2rating','rating_id','game_id');
    }

    function Board(){


        return $this->belongsTo('gamepedia\Models\Board','b_id');
    }

}