<?php

namespace gamepedia\Vue;

use gamepedia\Models\Platform;
use gamepedia\Models\Game;
use gamepedia\Models\Genre;
use gamepedia\Models\Game_rating;
use gamepedia\Models\game2genre;
use gamepedia\Models\Character;
use gamepedia\Models\Company;

class VueList{

    private $content;

    function __construct($tab)
    {
    $this->content=$tab;
    }

    public function render($id)
    {
        $app = \Slim\Slim::getInstance();
        $a=$app->urlFor("accueil");
        $s1=$app->urlFor("seance1");
        $s2=$app->urlFor("seance2");
        $s3=$app->urlFor("seance3");
        $cont="";
        switch ($id) {
            case 0:
                $chemin = "web/css";
                break;
            case 1:
                $chemin = "web/css";
                $cont = $this->seance1();
                break;
            case 2:
                $chemin = "web/css";
                $cont = $this->seance2();
                break;
            case 55:
                $chemin = "web/css";
                $cont = $this->Paginate();
                break;
        }

        //la page html
        $html = <<<END
                <!DOCTYPE html>
                <html lang="fr">
                    <head>
                        <title>Gamepedia</title>
                        <meta charset="utf-8">
                        <link rel="stylesheet" href="$chemin/homeGB.css">   
                    </head>
                    <body>
                        <header><a href=#><img src="$chemin/box_logo.png" alt="Gamepedia"></a></header>
                        <nav>
                            <ul id="menu">
                                <li class="linav"><a href='$a'>HOME</a></li>
                                <li class="linav"><a href='$s1'>seance1</a></li>
                                <li class="linav"><a href='$s2'>seance2</a></li>
                                <li class="linav"><a href='$s3'>seance3</a></li>
                            </ul>
                        </nav>                    
                        <div>
                            $cont
                        </div>                 
                        <footer>
                        </footer>
                    </body>
                </html>
END;
        return $html;
    }

    function seance1(){
        $cont="";
        $i=1;
        $y=0;
        foreach ($this->content as $tab) {
            $cont=$cont."<h1>requete $i</h1>";
            $y=1;
            foreach ($tab as $item) {

                $cont = $cont . <<<END
            <p>$y : $item->name: $item->deck </p>
END;
            $y++;
            }
            $i++;
        }
        return $cont;

    }
    function seance2(){
        $cont="";
        $y=1;
        $tab=$this->content[1];
        $cont=$cont."<h1>requete 1</h1>";
        foreach ($tab as $item) {

            $cont = $cont.<<<END
            <p>$y : $item->name: $item->deck </p>
END;
            $y++;
        }

        $req=$this->content[2];
        $cont=$cont."<h1>requete 2</h1>";
        foreach ($req as $game){
            $cont=$cont.<<<ENDe
            <h4> $game->name : </h4>
ENDe;
            $tmp = $game->characters()->get();
            $y=1;
            foreach ($tmp as $p){
                $cont = $cont . <<<END
            <p>$y : $p->name </p>
END;
                $y++;
            }
        }

        $req=$this->content[3];
        $cont=$cont."<h1>requete 3</h1>";
        foreach ($req as $comp) {
            $cont=$cont.<<<ENDe
            <h4> $comp->name : </h4>
ENDe;
            $res= $comp->games()->get();
            foreach ($res as $jeu){
                $cont=$cont.<<<ENDe
            <p> $jeu->name </p>
ENDe;
            }
        }


        //$q4 =Game::where('name','like','%Mario%')->get();
        $req=$this->content[4];
        $cont=$cont."<h1>requete 4</h1>";
        foreach ($req as $game) {

            $tmp = $game->gameRating()->get();

            foreach ($tmp as $jeu) {
                $cont=$cont.<<<ENDe
                <p>$jeu->name</p>
ENDe;
            }
        }

        $req=$this->content[5];
        $cont=$cont."<h1>requete 5</h1>";
        //$q5=Game::where('name','like','Mario%')->get();
        foreach ($req as $game) {
            $char = $game->characters()->get();
            if( count($char) >3){
                $cont=$cont.<<<ENDe
                <p>$game->name</p>
ENDe;
            }
        }
        $req=$this->content[6];
        $cont=$cont."<h1>requete 6</h1>";
       // $mario = Game::where('name','like','Mario%')->get();
        foreach ($req as $g) {
            $q6 = $g->gameRating()->where('name','like','%3+%')->get();
            foreach ($q6 as $rating){
                $cont=$cont.<<<ENDe
                <p>$game->name</p>
                
ENDe;
               // echo $rating->name."<br>";
            }
        }
        $req=$this->content[7];
        $cont=$cont."<h1>requete 7</h1>";
        $q7 = Game::where('name','like','Mario%')->get();
        foreach ($q7 as $g) {
            $q1=$g->comp()->where('name','like','%Inc.%')->get();
            $q2=$g->gameRating()->where('name','like','%3+%')->get();
            if((count($q1)>0)&&(count($q2)>0)){
                $cont=$cont.<<<ENDe
                <p>$g->name</p>
ENDe;
            }
            foreach ($q1 as $C){
                //echo $C->name."<br>";
            }

        }
        $req=$this->content[8];
        $cont=$cont."<h1>requete 8</h1>";
        $q8 = Game::where('name','like','Mario%')->get();
        foreach ($q8 as $g) {
            $q1=$g->comp()->where('name','like','%Inc.%')->get();
            $q2=$g->gameRating()->where('name','like','%3+%')->get();
            $cero=false;
            foreach ($q2 as $R){
                $q3=$R->Board()->where('name','like','CERO')->get();
                if(count($q1)>0){
                    $cero=true;
                }
            }

            if((count($q1)>0)&&(count($q2)>0)&&($cero)){
                $cont=$cont.<<<ENDe
                <p>$g->name</p>
ENDe;
            }
            foreach ($q1 as $C){
               // echo $C->name."<br>";
            }

        }
        return $cont;
    }

    function paginate(){
        $app = \Slim\Slim::getInstance();
        $next=$app->urlFor('lister');
        $cont="";
        $y=1;
        foreach ($this->content as $item) {

                $cont = $cont . <<<END
            <p>$y : $item->name: $item->deck </p>
END;
                $y++;
        }
        $cont = $cont .($this->content->render());
        return $cont;
    }


}

