<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 27/02/2017
 * Time: 16:48
 */
namespace gamepedia\Controlleur;

use gamepedia\Models\Platform;
use gamepedia\Vue\VueList;
use gamepedia\Models\Game;
use gamepedia\Models\Company;

class ControlerList
{
    public function __construct()
    {

    }

    public function lister()
    {
        $game = Game::where('name', 'LIKE', '%Mario%')->get();
        $company = Company::where('location_country', '=', 'Japan')->get();
        $platform = Platform::where('install_base', '>=', 10000000)->get();
        $list = Game::where('id', '>=', 21173)->take(442)->get();
        $listGames = Game::select('name', 'deck')->paginate(500);

        $tab = array(1 => $game, 2 => $company, 3 => $platform,
            4 => $list,5 => $listGames);

       $vue = new VueList($tab);
        print $vue->render(1);
    }


}