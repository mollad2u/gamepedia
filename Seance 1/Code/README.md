Sch�ma relationnel :

*** Version Texte ***

Jeux (id , nom, descripc, descripl, dateinit, dateatt)
Th�me (id, nom)
CorresTh�me (idJeux, idTh�me)
Genre (id, nom, descripc, descripl)
CorresGenre (idJeux, idGenre)
Plateforme (id, nom, alias, descripc, descripl, date, tarif, base, idCompagnie)
Compagnie (id, nom, alias, abrev, descripc, descripl, adresse, ville, pays, date, tel, site)
Developper (idJeux, idCompagnie)
Publier (idJeux, idCompagnie)
Classement (idJeux, idOrga, age)
Organisme (id, nom, descripc, descripl)
Personnage (id, nom, alias, nomreel, nomfamille, naiss, genre, descripc, descripl, p_apparition)
Utiliser (idJeux, idPerso)
Ami (idPerso1, idPerso2)
Ennemi  (idPerso1, idPerso2)

-------------------------------------

*** Version Cours ***

game (id , name, alias, deck, description, expected_release_date, original_release_date, created_at, updated_at)
theme (id, name)
corres_theme (#id_game, #id_theme)
genre (id, name, deck, description)
corres_genre (#id_game, #id_genre)
platform (id, name, alias, abbreviation, deck, description, installation_base, release_date, online_support, original_price)
producer(#id_platform, #id_company)
company (id, name, alias, abbreviation, deck, description, date_founded , location_address, phone, website)
developers (#id_game, #id_company)
publishers (#id_game, #id_company)
game_rating (id, name, #id_rating_board)
original_game_ratings(#id_game_rating, #id_game)
rating_board (id, name, deck, description)
character (id, name, real_name, last_name, alias, birthday, gender, deck, description, created_at, updated_at, #id_game)
appears_in (#id_game, #id_character)
friends (#id_character1, #id_character2)
enemies  (#id_character1, #id_character2)
similar_games (#id_game1, #id_game2)
