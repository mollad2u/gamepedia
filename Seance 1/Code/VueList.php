<?php

namespace gamepedia\Vue;

class VueList
{

    private $content;

    function __construct($tab)
    {
    $this->content=$tab;
    }

    public function render($id)
    {
        switch ($id) {
            case 1:
                $chemin = "web/css";
                $cont = $this->Partie2();
                break;
        }

        //la page html
        $html = <<<END
                <!DOCTYPE html>
                <html lang="fr">
                    <head>
                        <title>Gamepedia</title>
                        <meta charset="utf-8">
                        <link rel="stylesheet" href="$chemin/homeGB.css">   
                    </head>
                    <body>
                        <header><a href=#><img src="$chemin/box_logo.png" alt="logoGiftBox"></a></header>
                        <nav>
                            <ul id="menu">
                                <li class="linav"><a href=#>HOME</a></li>
                                <li class="linav"><a href=>CATEGORIE</a>
                            <ul>
                            </ul></li>
                                <li class="linav"><a href=#>PRESTATION</a></li>
                                <li class="linav"><a href=#>COFFRET</a></li>
                                <li class="linav"><a href=#></a></li>
                            </ul>
                        </nav>                    
                        <div>
                            $cont
                        </div>                 
                        <footer>
                        </footer>
                    </body>
                </html>
END;
        return $html;
    }

    function Partie2(){
        $cont="";
        $i=1;
        $y=0;
        foreach ($this->content as $tab) {
            $cont=$cont."<h1>requete $i</h1>";
            $y=1;
            foreach ($tab as $item) {

                $cont = $cont . <<<END
            <p>$y : $item->name: $item->deck </p>
END;
            $y++;
            }
            $i++;
        }
        return $cont;

    }

}

