<?php
/**
 * Created by PhpStorm.
 * User: gauth
 * Date: 27/02/2017
 * Time: 15:50
 */
require 'vendor/autoload.php';
use \gamepedia\Controlleur\ControlerList;
use  \Illuminate\Database\Capsule\Manager as db;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//conection a la base de donnée
$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
db::connection()->enableQueryLog();


$app2 = new \Slim\App;
$app2 ->get('/api/games', function ( $request,  $response,$args) use($app2){
    $myvar1 = $request->getParam('page');
    (new ControlerList($app2))->games($myvar1,$response);
    return $response;
})->setName('games');
$app2->get('/api/games/{id}/characters',function($request, $response, $args)use($app2){(new ControlerList($app2))->characters($args['id'],$response);})->setName('characters');
$app2->get('/api/plateform/{id}',function($request, $response, $args)use($app2){(new ControlerList($app2))->Platform($args['id'],$response);})->setName('Platform');
$app2->get('/api/games/{id}',function($request, $response, $args)use($app2){(new ControlerList($app2))->game($args['id'],$response);})->setName('game');
$app2->get('/api/games/{id}/comments',function($request, $response, $args)use($app2){(new ControlerList($app2))->Comments($args['id'],$response);})->setName('comments');
$app2->run();

/*$app=new \Slim\App;
//chose utile pour la gestion du coffret
//$_SESSION['panier'][]=;
//unset($_SESSION['prixPanier']);

$app->get('/',function(){(new ControlerList())->accueil();})->setName('accueil');
$app->get('/Seance1',function(){(new ControlerList())->Seance1(); $query = db::getQueryLog();
    var_dump($query);})->setName('seance1');
$app->get('/Seance2',function(){(new ControlerList())->Seance2();$query = db::getQueryLog();
    var_dump($query);})->setName('seance2');
$app->get('/Seance3',function(){(new ControlerList())->Seance3();$query = db::getQueryLog();
    var_dump($query);})->setName('seance3');
$app->get('/Seance4',function(){(new ControlerList())->Seance4();$query = db::getQueryLog();
    var_dump($query);})->setName('seance4');
$app->get('/Seance5',function(){(new ControlerList())->Seance5();})->setName('seance5');
$app->get('/api/games',function(){(new ControlerList())->games();})->setName('games');
$app->get('/api/games/:id',function($id){(new ControlerList())->game($id);})->setName('game');

$app->run();
*/
