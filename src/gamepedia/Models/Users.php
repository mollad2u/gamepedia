<?php
namespace gamepedia\Models;

class Users extends \Illuminate\Database\Eloquent\Model {

    protected $table = "users";
    protected $primaryKey = "id";
    public $timestamps = false;
    function  comments(){
        return $this->belongsToMany('gamepedia\Models\Commentary','user_comment','User_id','coment_id');
    }

}