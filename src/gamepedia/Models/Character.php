<?php

namespace gamepedia\Models;


class Character extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'character';
    protected $primaryKey = 'id';
    public $timestamps = false;
	
	

function gameFirst(){  
	return $this->belongsTo('gamepedia\Models\Game',"character","first_appear_in_game_id");
}

function games(){
    return $this->belongToMany('gamepedia\Models\Game','game2character','character_id','game_id' );
}

function enemies(){
	return $this->hasMany('gamepedia\Models\Character', "enemies","char1_id", "char2_id");
}
	
function friends(){	
	return $this->hasMany('gamepedia\Models\Character', "friends","char1_id", "char2_id");
}

}
