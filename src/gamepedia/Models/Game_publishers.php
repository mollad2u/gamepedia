<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 27/02/2017
 * Time: 16:31
 */

namespace gamepedia\Models;

class Game_publishers extends \Illuminate\Database\Eloquent\Model {

    protected $table = "game_publishers";
    protected $primaryKey = "game_id,comp_id";
    public $timestamps = false;


}