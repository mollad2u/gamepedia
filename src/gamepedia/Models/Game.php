<?php
/**
 * Created by PhpStorm.
 * User: gauth
 * Date: 27/02/2017
 * Time: 16:32
 */
namespace gamepedia\Models;


class Game extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;

	function comp(){
	    return $this->belongsToMany('gamepedia\Models\Company', 'game_developers' ,'game_id' ,'comp_id');
    }
	function developpers(){
		return $this->belongToMany('gamepedia\Models\Company', "game_developers","game_id", "comp_id");
	}
	
	function publishers(){
		return $this->belongToMany('gamepedia\Models\Company', "game_publishers","game_id", "comp_id");
	}

	function plateform(){
        return $this->belongsToMany('gamepedia\Models\Platform', "game2platform","game_id", "platform_id");
	}

	function game2theme(){
		return $this->belongsToMany('gamepedia\Models\Theme', "game2theme","game_id", "theme_id");
	}
	
	function characterFirst(){
		return $this->hasMany('gamepedia\Models\Character', "first_appear_in_game_id");
	}

    function characters(){
        return $this->belongsToMany('gamepedia\Models\Character', 'game2character', 'game_id', 'character_id');
    }

    function gameRating(){
        return $this->belongsToMany('gamepedia\Models\Rating', 'game2rating','game_id', 'rating_id');
    }

	function game2genre(){
		return $this->belongsToMany('gamepedia\Models\Genre', 'game2genre', 'game_id', 'genre_id');
	}
	
	function similarGame(){
		return $this->belongsToMany('gamepedia\Models\Game', 'similar_games', 'game1_id', 'game2_id');
	}
    function  Comments(){
        return $this->belongsToMany('gamepedia\Models\Commentary','game_comment','game_id','coment_id');
    }
}