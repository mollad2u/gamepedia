<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 27/02/2017
 * Time: 16:30
 */

namespace gamepedia\Models;

class Game_developers extends \Illuminate\Database\Eloquent\Model {

    protected $table = "game_developers";
    protected $primaryKey = "game_id,comp_id";
    public $timestamps = false;


}