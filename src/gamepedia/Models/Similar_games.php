<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 27/02/2017
 * Time: 16:36
 */
namespace gamepedia\Models;

class Similar_games extends \Illuminate\Database\Eloquent\Model {

    protected $table = "similar_games";
    protected $primaryKey = "game1_id, game2_id";
    public $timestamps = false;

}