<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 27/02/2017
 * Time: 16:35
 */

namespace gamepedia\Models;
class Platform extends \Illuminate\Database\Eloquent\Model {

    protected $table = "platform";
    protected $primaryKey = "id";
    public $timestamps = false;

}