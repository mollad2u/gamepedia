<?php
/**
 * Created by PhpStorm.
 * User: gauth
 * Date: 13/03/2017
 * Time: 07:55
 */

namespace gamepedia\Models;

class Game2genre extends \Illuminate\Database\Eloquent\Model {

    protected $table = "game2genre";
    protected $primaryKey = "game_id,genre_id";
    public $timestamps = false;


}