<?php
/**
 * Created by PhpStorm.
 * User: gauth
 * Date: 27/02/2017
 * Time: 16:27
 */

namespace gamepedia\Models;


class Enemies extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'enemies';
    protected $primaryKey = 'char1_id ,char2_id';
    public $timestamps = false;

}