<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 27/02/2017
 * Time: 16:37
 */
namespace gamepedia\Models;

class Theme extends \Illuminate\Database\Eloquent\Model {

    protected $table = "theme";
    protected $primaryKey = "id";
    public $timestamps = false;

}