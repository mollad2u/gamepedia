<?php
namespace gamepedia\Models;

class Commentary extends \Illuminate\Database\Eloquent\Model {

    protected $table = "commentary";
    protected $primaryKey = "id";
    public $timestamps = true;

    function games(){
        return $this->belongToMany('gamepedia\Models\Game','game_comment','coment_id','game_id' );
    }
    function USERS(){
        return $this->belongToMany('gamepedia\Models\Users','user_comment','Coment_id','User_id' );
    }
}