<?php

namespace gamepedia\Models;


class Friends extends \Illuminate\Database\Eloquent\Model{

protected $table = 'friends';
protected $primaryKey = 'char1_id ,char2_id';
public $timestamps = false;

}