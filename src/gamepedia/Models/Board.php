<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 27/02/2017
 * Time: 16:36
 */

namespace gamepedia\Models;
class Board extends \Illuminate\Database\Eloquent\Model {

    protected $table = "rating_board";
    protected $primaryKey = "id";
    public $timestamps = false;

}