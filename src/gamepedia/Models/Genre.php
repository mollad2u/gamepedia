<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 27/02/2017
 * Time: 16:34
 */

namespace gamepedia\Models;

class Genre extends \Illuminate\Database\Eloquent\Model {

    protected $table = "genre";
    protected $primaryKey = "id";
    public $timestamps = false;

}