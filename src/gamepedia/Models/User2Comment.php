<?php
/**
 * Created by PhpStorm.
 * User: gauth
 * Date: 20/03/2017
 * Time: 15:10
 */

namespace gamepedia\Models;


class User2Comment extends \Illuminate\Database\Eloquent\Model {

    protected $table = "user_comment";
    protected $primaryKey = "User_id ,coment_id";
    public $timestamps = false;
}