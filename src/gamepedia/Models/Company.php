<?php

namespace gamepedia\Models;


class Company extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'company';
    protected $primaryKey = 'id';
    public $timestamps = false;

    function games(){

        return $this->belongsToMany('gamepedia\Models\Game','game_developers','comp_id','game_id');
    }
}

