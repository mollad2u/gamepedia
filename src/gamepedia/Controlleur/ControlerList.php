<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 27/02/2017
 * Time: 16:48
 */
namespace gamepedia\Controlleur;

use Faker\Factory;
use gamepedia\Models\Users;
use gamepedia\Models\Commentary;
use gamepedia\Models\Game2Comment;
use gamepedia\Models\User2Comment;
use Illuminate\Support\Facades\DB;
use gamepedia\Models\Platform;
use gamepedia\Vue\VueList;
use gamepedia\Models\Game;
use gamepedia\Models\Genre;
use gamepedia\Models\Game_rating;
use gamepedia\Models\game2genre;
use gamepedia\Models\Character;
use gamepedia\Models\Company;


class ControlerList
{
    private $app;

    public function __construct($a)
    {
        $this->app=$a;
    }
    public function accueil(){
        $vue = new VueList("a");
        print $vue->render(0);
    }
    public function Seance1()
    {
        $game = Game::where('name', 'LIKE', '%Mario%')->get();
        $company = Company::where('location_country', '=', 'Japan')->get();
        $platform = Platform::where('install_base', '>=', 10000000)->get();
        $list = Game::where('id', '>=', 21173)->take(442)->get();
        $listGames = Game::select('name', 'deck')->paginate(500);


        $tab = array(1 => $game, 2 => $company, 3 => $platform,
            4 => $list,5 => $listGames);

       $vue = new VueList($tab);
        print $vue->render(1);
    }

    public function Seance2(){

        $id = Game::find('12342');
        $perso =$id->characters()->get();
        $req =Game::where('name','like','Mario%')->get();
        $q3 = Company::where('name','like','%Sony%')->get();
        $q4 =Game::where('name','like','%Mario%')->get();
        $q5=Game::where('name','like','Mario%')->get();
        $q6 = Game::where('name','like','Mario%')->get();
        $q7 = Game::where('name','like','Mario%')->get();
        $q8 = Game::where('name','like','Mario%')->get();
        $p=new Genre();
        $p->save();

        $q1=new Game2genre();
        $q1->genre_id=($p->id);
        $q1->game_id=12;
        $q1->save();

        $q2=new Game2genre();
        $q2->game_id=56;
        $q2->genre_id=($p->id);
        $q2->save();

        $q10=new Game2genre();
        $q10->genre_id=($p->id);
        $q10->game_id=12;
        $q10->save();

        $q11=new Game2genre();
        $q11->genre_id=($p->id);
        $q11->game_id=345;
        $q11->save();


        $tab = array(1 => $perso, 2 => $req, 3 => $q3,4 => $q4,5 => $q5, 6=>$q6, 7=>$q7, 8=> $q8);
        $vue = new VueList($tab);
        print $vue->render(2);

    }

    public function Seance3(){
        /*$vue = new VueList();
        print $vue->render(3);*/
		// Partie 1 :
		/* 1. lister l'ensemble des jeux
		  ini_set('memory_limit','-1');
		  $time_debut = microtime(true);
		  $game = Game::get();
		  $time_fin = microtime(true);
		  $difference = $time_fin - $time_debut;
		  echo "Le temps d'exécution est de : " . $difference;//*/

        //2. lister les jeux dont le nom contient 'Mario'

        /*  ini_set('memory_limit','-1');
          $time_debut = microtime(true);
          $game = Game::where('name', 'LIKE', 'Mario%')->get();
          $time_fin = microtime(true);
          $difference = $time_fin - $time_debut;
          echo "Le temps d'exécution est de : " . $difference;//*/


        /* // 3. afficher les personnages des jeux dont le nom débute par 'Mario'
          ini_set('memory_limit','-1');
          $time_debut = microtime(true);
          $perso= game::where('name', 'LIKE', 'Mario%')->get();
          foreach ($perso as $game) {
                    $char = $game->characters()->get();
                  }
          $time_fin = microtime(true);
          $difference = $time_fin - $time_debut;
          echo "Le temps d'exécution est de : " . $difference;//*/

      /*  // 4. les jeux dont le nom débute par 'Mario' et dont le rating initial contient '3+'
        ini_set('memory_limit','-1');
        $time_debut = microtime(true);
        $q6 = Game::where('name','like','Mario%')->get();
        foreach ($q6 as $g) {
            $m = $g->gameRating()->where('name','like','%3+%')->get();
        }

        $time_fin = microtime(true);
        $difference = $time_fin - $time_debut;
        echo "Le temps d'exécution est de : " . $difference;
        $c=$_GET['country'];
        $time_debut = microtime(true);
        $c=Company::where('location_country','like',$c)->get();
        $time_fin = microtime(true);
        $difference = $time_fin - $time_debut;
        echo "Le temps d'exécution est de : " . $difference;
		*/
		//Mettre sa dans le sql
        //SET GLOBAL general_log = 'ON';
        //SET GLOBAL log_output = 'TABLE';
		//Partie 2:
		//1. lister les jeux dont le nom contient Mario
		ini_set('memory_limit','-1');
		//$mario=game::where('name','LIKE','%Mario%')->get();
		//var_dump($mario);
		//2. afficher le nom des personnages du jeu 12342
		//$characters=game::where('id','=',12342)->first()->characters();
		//var_dump($characters);
		//3. afficher les noms des persos apparus pour la 1ère fois dans 1 jeu dont le nom contient Mario
		//$mario=game::where('name','LIKE','%Mario%')->get();
		/*foreach($mario as $m){
			$first_characters=$m->characterFirst()->get();
			var_dump($first_characters);
		}*/
		//4. afficher le nom des personnages des jeux dont le nom contient 'Mario'
		/*$mario=game::where('name','LIKE','%Mario%')->get();
		foreach($mario as $m){
			$characters=$m->characters()->get();
			foreach($characters as $c){
				echo $c->name.'</br>';
			}
		}*/
		//5. les jeux développé par une compagnie dont le nom contient Sony%
		//$sony=company::where('name','LIKE','%Sony%')->get();
		/*foreach($sony as $s){
			$games=$s->games()->get();
			foreach($games as $g){
				echo $g->name.'</br>';
			}
		}*/
		
    }

    /**
     *
     */
    public function Seance4(){

/*
        $user1=new Users();
        $user1->nom="bernard";
        $user1->prenom="a";
        $user1->adresse="150 avenue des shorts";
        $user1->tel="06157835**";
        $user1->email="Abernard@gmail.com";
        $user1->date_naissance= \DateTime::createFromFormat('m/d/Y', '1/10/2014');

        $user2= new Users();
        $user2->nom="Maurice";
        $user2->prenom="o";
        $user2->adresse="151 avenue des shorts";
        $user2->tel="061565**";
        $user2->email="Omaurice@gmail.com";
        $user2->date_naissance= \DateTime::createFromFormat('m/d/Y', '1/10/2014');

        $user1->save();
        $user2->save();

        //A REFAIRE
        $com1= new Commentary();
        $com1->contenu="ce jeu est trop viloent";
        $com2= new Commentary();
        $com2->contenu="un jeu parfait pour les enfant";
        $com3= new Commentary();
        $com2->contenu="mon papi a adoré ce jeu, bravo les developeurs";
        $com1->save();
        $com2->save();
        $com3->save();
        $com4= new Commentary();
        $com4->contenu="ce jeu est trop biern";
        $com5= new Commentary();
        $com5->contenu="un jeu parfait pour les adult";
        $com6= new Commentary();
        $com6->contenu="mon chien a adoré ce jeu, bravo les developeurs";
        $com4->save();
        $com5->save();
        $com6->save();

        $uc=new User2Comment();
        $uc->User_id =$user1->id;
        $uc->Coment_id =$com1->id;
        $uc->save();
        $uc2=new User2Comment();
        $uc2->User_id =$user1->id;
        $uc2->Coment_id =$com2->id;
        $uc2->save();
        $uc3=new User2Comment();
        $uc3->User_id =$user1->id;
        $uc3->Coment_id =$com3->id;
        $uc3->save();

        $uc4=new User2Comment();
        $uc4->User_id =$user2->id;
        $uc4->Coment_id =$com4->id;
        $uc4->save();
        $uc5=new User2Comment();
        $uc5->User_id =$user2->id;
        $uc5->Coment_id =$com5->id;
        $uc5->save();
        $uc6=new User2Comment();
        $uc6->User_id =$user2->id;
        $uc6->Coment_id =$com6->id;
        $uc6->save();


        $gc=new Game2Comment();
        $gc->game_id =12342;
        $gc->Coment_id =$com1->id;
        $gc->save();
        $gc2=new Game2Comment();
        $gc2->game_id =12342;
        $gc2->Coment_id =$com2->id;
        $gc2->save();
        $gc3=new Game2Comment();
        $gc3->game_id =12342;
        $gc3->Coment_id =$com3->id;
        $gc3->save();

        $gc5=new Game2Comment();
        $gc5->game_id =12342;
        $gc5->Coment_id =$com4->id;
        $gc5->save();
        $gc4=new Game2Comment();
        $gc4->game_id =12342;
        $gc4->Coment_id =$com5->id;
        $gc4->save();
        $gc6=new Game2Comment();
        $gc6->game_id =12342;
        $gc6->Coment_id =$com6->id;
        $gc6->save();

        $com=Commentary::all();
        foreach ($com as $co){
            echo $co->contenu."<br>";
        }
        $com=Game::find('12342');
        echo  $com->name."<br>";
        $a=$com->Comments()->get();
        foreach ($a as $c){
            echo $c->contenu."<br>";
        }
        echo "<br>";
        */
        /*
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time',-1);
        $faker = Factory::create();
        for ($i=0; $i < 25000; $i++) {
            $user = new Users();
            $user -> nom = $faker->firstName;
            $user -> prenom = $faker->lastName;
            $user -> adresse = $faker->address;
            $user -> tel = $faker->phoneNumber;
            $user -> email = $faker->email;
            $user -> date_naissance = $faker->dateTimeThisCentury->format('Y-m-d');
            $user->Save();
            $y=0;
            while($y<10) {
                $commentaire1 = new Commentary();
                $commentaire1->contenu = $faker->text;
                $commentaire1->created_at = $faker->dateTimeThisCentury->format('Y-m-d');
                $commentaire1->updated_at = $faker->dateTimeThisCentury->format('Y-m-d');
                $commentaire1->save();
                $uc4=new User2Comment();
                $uc4->User_id =$user->id;
                $uc4->Coment_id =$commentaire1->id;
                $uc4->save();
                $gc3=new Game2Comment();
                $gc3->game_id =$faker->randomDigit;
                $gc3->Coment_id =$commentaire1->id;
                $gc3->save();
                $y++;
            }
        }*/
        $u= Users::find('12342');
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time',-1);
        $us=Users::all();

        $tab = array(1 => $u, 2 => $us);
        $vue = new VueList($tab);
        print $vue->render(4);
    }

    public function Seance5(){
        $vue = new VueList();
        print $vue->render(5);
    }

    public function game($id,$r){
        $r->withHeader('Content-type', 'application/json');

        $a=Game::find($id);
        $p=$a->plateform()->get();
        $platforms=[];
        foreach ($p as $plat){
           $console = array('id'=>$plat->id, 'name' => $plat->name, 'alias'=>$plat->alias,'abreviation'=>$plat->abbreviation);
            $platforms[]=array('plateform' => $console, 'links'=>array('self' =>array("href"=>"/api/plateform/".$plat->id)));
        }
        $arr = array('id' => $a->id, 'name' => $a->name, 'alias' => $a->alias, 'deck' => $a->deck,'original_release_date'=> $a->original_release_date );
        $res=array('games' => $arr,'plateforms'=>$platforms, 'links'=>array('comments' =>array("href"=>"/api/games/".$id."/comments"),'characters' =>array("href"=>"/api/games/".$id."/characters")));
        echo json_encode($res, JSON_UNESCAPED_SLASHES);

;
    }
    public function Platform ($id,$r){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time',-1);
        $r->withHeader('Content-type', 'application/json');
        $a=Game::find($id);
        $a=$a->plateform()->get();
        foreach ($a as $plat) {
            $console = array('id'=>$plat->id, 'name' => $plat->name, 'alias'=>$plat->alias,'abreviation'=>$plat->abbreviation);
            $arr=array('plateform'=>$console);

        }
        echo json_encode($arr, JSON_UNESCAPED_SLASHES);
    }
    public function characters ($id,$r){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time',-1);
        $r->withHeader('Content-type', 'application/json');
        $a=Game::find($id);
        $a=$a->characters()->get();
        $res=[];
        foreach ($a as $plat) {
            $fa=Game::find($plat->first_appeared_in_game_id);
            $console = array('id'=>$plat->id, 'name' => $plat->name, 'real_name'=>$plat->real_name,'last_name'=>$plat->last_name, 'alias '=>$plat->alias ,'birthday'=>$plat->birthday ,'gender '=>$plat->gender ,'deck'=>$plat->deck, 'first_appeared_in_game_id'=>$fa->name ,'created_at '=>$plat->created_at  ,'updated_at'=>$plat->updated_at);
            $res[]=array('character'=>$console);

        }
        $res=array('characters' => $res);
        echo json_encode($res, JSON_UNESCAPED_SLASHES);
    }

    public function Comments($id,$r){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time',-1);
        $r->withHeader('Content-type', 'application/json');
        $a=Game::find($id);
        $a=$a->Comments()->get();
        $res=[];
        $user="";
        foreach ($a as $c) {

            $arr = array('id' => $c->id, 'contenu' => $c->contenu, 'created_at' => $c->created_at, 'updated_at' => $c->updated_at);
            $arr=array('comment'=>$arr,'user'=>$user);
            $res[]=$arr;
        }
        $res=array('Commentary' => $res);
        echo json_encode($res, JSON_UNESCAPED_SLASHES);
    }
    public function games($p,$r){
        $r->withHeader('Content-type', 'application/json');
        $g=Game::take(200)->skip(200*$p)->get();
        $res=[];
        foreach ($g as $a) {
            $arr = array('id' => $a->id, 'name' => $a->name, 'alias' => $a->alias, 'deck' => $a->deck,/* 'description' => $a->description,*/ 'original_release_date' => $a->original_release_date);
            $arr=array('game'=>$arr,'links'=>array("href" => '/api/games/'. $a->id));
            $res[]=$arr;
        }
        $prev=$p-1;
        $next=$p+1;
        $res=array('games' => $res, 'links'=>array('prev' =>array("href"=>"/api/games?page=$prev"),'next' =>array("href"=>"/api/games?page=$next")));
        echo json_encode($res, JSON_UNESCAPED_SLASHES);
        //$vue = new VueList($arr);
        //print $vue->render(6);
    }

}
