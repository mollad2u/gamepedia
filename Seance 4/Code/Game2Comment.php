<?php
/**
 * Created by PhpStorm.
 * User: gauth
 * Date: 20/03/2017
 * Time: 15:08
 */

namespace gameped1ia\Models;


class Game2Comment extends \Illuminate\Database\Eloquent\Model {

    protected $table = "game_comment";
    protected $primaryKey = "game_id,coment_id";
    public $timestamps = false;
}